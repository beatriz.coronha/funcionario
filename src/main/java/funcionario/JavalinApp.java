package funcionario;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import funcionario.controllers.FuncionarioController;
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

public class JavalinApp {

	   private Javalin app = 
	    		Javalin.create(config -> {
	                config.registerPlugin(getConfiguredOpenApiPlugin());
	                config.defaultContentType = "application/json";
	            }).routes(() -> {
	                path("funcionario", () -> {
	                    get(FuncionarioController::getFuncionario);
	                    delete(FuncionarioController::deleteFuncionario);
	                    post(FuncionarioController::createFuncionario);
	                });
	                path("consertaBicicleta", () -> post(FuncionarioController::consertaBicicleta));
	                path("consertaTotem", () -> post(FuncionarioController::consertaTotem));
	                path("consertaTranca", () -> post(FuncionarioController::consertaTranca));
	                path("aposentaBicicleta", () -> post(FuncionarioController::aposentaBicicleta));
	                path("aposentaTotem", () -> post(FuncionarioController::aposentaTotem));
	                path("aposentaTranca", () -> post(FuncionarioController::aposentaTranca));
	            });
	   
	   public void start (int port) {
		   this.app.start(port);
	   }
	   
	   public void stop() {
		   this.app.stop();
	   }

	    static OpenApiPlugin getConfiguredOpenApiPlugin() {
	        Info info = new Info().version("1.0").description("User API");
	        OpenApiOptions options = new OpenApiOptions(info)
	        		.activateAnnotationScanningFor("funcionario")
	                .path("/swagger-docs") // endpoint for OpenAPI json
	                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
	                .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
	                .defaultDocumentation(doc -> {
	                    doc.json("400", ErrorResponse.class);
	                });
	        return new OpenApiPlugin(options);
	    }
	   
	}
