package funcionario.dtos;

import java.util.Date;

public class LogRetiradaDto {
	public Date dataRetirada;
	public long matriculaFuncionario;
	public long numero;
	
	public LogRetiradaDto(Date dataRetirada, long matriculaFuncionario, long numero) {
		this.dataRetirada = dataRetirada;
		this.matriculaFuncionario = matriculaFuncionario;
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "LogRetiradaDto [dataRetirada=" + dataRetirada + ", matriculaFuncionario=" + matriculaFuncionario
				+ ", numero=" + numero + "]";
	}

}
