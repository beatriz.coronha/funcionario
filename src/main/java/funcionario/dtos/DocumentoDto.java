package funcionario.dtos;

public class DocumentoDto {
	public String tipo;
	public long numero;
	public String foto;
	
	public DocumentoDto() {
		
	}
	
	public DocumentoDto(String tipo, long numero, String foto){
		this.tipo = tipo;
		this.numero = numero;
		this.foto = foto;
	}

}
