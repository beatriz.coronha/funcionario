package funcionario.dtos;

public class FuncionarioDto {
	public String id;
	public String nome;
	public long matricula;
	public boolean indicadorBr;
	public DocumentoDto documento;
	
	public FuncionarioDto(){
		
	}
	
	public FuncionarioDto(String id, String nome, long matricula, boolean indicadorBr, DocumentoDto documento) {
		this.id = id;
		this.nome = nome;
		this.matricula = matricula;
		this.indicadorBr = indicadorBr;
		this.documento = documento;
	}
}