package funcionario.services;

import funcionario.daos.LogRetiradaDao;
import funcionario.dtos.FuncionarioDto;
import funcionario.dtos.LogRetiradaDto;

public class OperacoesService {
	
	private OperacoesService(){
	}
	
	public static LogRetiradaDto saveRetirada(int idFuncionario, long numero)
	{
		FuncionarioDto funcionario = FuncionarioService.findById(idFuncionario);
		return LogRetiradaDao.save(funcionario.matricula, numero);
	}


}
