package funcionario.services;

import java.util.Collection;
import java.util.Map;

import funcionario.NewFuncionarioRequest;
import funcionario.daos.FuncionarioDao;
import funcionario.dtos.FuncionarioDto;

public class FuncionarioService {
	
	private FuncionarioService() {
		
	}

    public static FuncionarioDto findById(int id) {
        return FuncionarioDao.getById(id);
    }
	
	public static void saveFuncionario(NewFuncionarioRequest funcionario) {
		FuncionarioDao.save(funcionario);		
	}
	
	public static void deleteFuncionario(int id)
	{
		FuncionarioDao.delete(id);		
	}
	
	public static FuncionarioDto findByMatricula(long matricula)
	{
		Map<Integer, FuncionarioDto> funcionarios =  FuncionarioDao.getFuncionarios();
		FuncionarioDto funcionarioDto = null;
		
		for(FuncionarioDto funcionario : funcionarios.values())
		{
			if(funcionario.matricula == matricula)
			{
				funcionarioDto = funcionario;
			}
		}
		return funcionarioDto;
	}
	
	public static FuncionarioDto findByNome(String nome)
	{
		Map<Integer, FuncionarioDto> funcionarios =  FuncionarioDao.getFuncionarios();
		FuncionarioDto funcionarioDto = null;
		
		for(FuncionarioDto funcionario : funcionarios.values())
		{
			if(funcionario.nome.equals(nome))
			{
				funcionarioDto = funcionario;
			}
		}
		return funcionarioDto;
	}
	
	public static Collection<FuncionarioDto> getAllFuncionarios()
	{
		return FuncionarioDao.getAll();
		
	}
	


}
