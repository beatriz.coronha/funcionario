package funcionario.daos;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import funcionario.NewFuncionarioRequest;
import funcionario.dtos.FuncionarioDto;

public class FuncionarioDao {
	
    private static Map<Integer, FuncionarioDto> funcionarios = new HashMap<>();
	private static AtomicInteger lastId;
    static {
    	funcionarios.put(1, new FuncionarioDto("1", "Alice", 000, true, null));
    	funcionarios.put(2, new FuncionarioDto("2", "Bob", 111, true, null));
    	funcionarios.put(3, new FuncionarioDto("3", "Carol", 222, true, null));
    	funcionarios.put(4, new FuncionarioDto("4", "Dave", 333, true, null));
        lastId = new AtomicInteger(funcionarios.size());
    }
	
	FuncionarioDao() {
	}
	
    public static Map<Integer, FuncionarioDto> getFuncionarios() {
		return funcionarios;
	}
	public static void setFuncionarios(Map<Integer, FuncionarioDto> funcionarios) {
		FuncionarioDao.funcionarios = funcionarios;
	}
	public static AtomicInteger getLastId() {
		return lastId;
	}
	public static void setLastId(AtomicInteger lastId) {
		FuncionarioDao.lastId = lastId;
	}
	
	public static void save(NewFuncionarioRequest funcionario)
	{
        int id = lastId.incrementAndGet();
		FuncionarioDto funcDto = new FuncionarioDto(
										String.valueOf(id),
										funcionario.nome,
										funcionario.matricula,
										funcionario.indicadorBr,
										funcionario.documento);
        funcionarios.put(id, funcDto);
	}
	
	public static FuncionarioDto getById(int id)
	{
        return funcionarios.get(id);	
	}
	
	public static void delete(int id)
	{
		funcionarios.remove(id);
	}
	
	public static Collection<FuncionarioDto> getAll()
	{
		return funcionarios.values();
	}
	
}