package funcionario.daos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import funcionario.dtos.LogRetiradaDto;

public class LogRetiradaDao {
    private static List<LogRetiradaDto> logsRetirada = new ArrayList<>();
	
    LogRetiradaDao() {
	}
    
	public static LogRetiradaDto save(long matricula, long numero)
	{
		LogRetiradaDto logRetirada = new LogRetiradaDto(
										new Date(),
										matricula,
										numero);
		logsRetirada.add(logRetirada);
		return logRetirada;
	}

	public static List<LogRetiradaDto> getLogsRetirada() {
		return logsRetirada;
	}



}
