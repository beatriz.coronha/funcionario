package funcionario.controllers;

import java.util.ArrayList;
import java.util.Collection;

import funcionario.ErrorResponse;
import funcionario.NewFuncionarioRequest;
import funcionario.dtos.FuncionarioDto;
import funcionario.dtos.LogRetiradaDto;
import funcionario.services.FuncionarioService;
import funcionario.services.OperacoesService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import mocksmicrosservicos.Bicicleta;
import mocksmicrosservicos.Totem;
import mocksmicrosservicos.Tranca;

public class FuncionarioController {
	private static final String USER_NOT_FOUND = "User not found";
	private static final String EM_REPARO = "Em Reparo";	
	private static final String APOSENTADA = "Aposentada";
	private static final String EMAIL_TO_SEND = "beatriz.coronha@uniriotec.br";
	private static final String URL_HEROKU_EMAIL = "https://scbintegracoes.herokuapp.com/enviaEmail";
	private static final String EMAIL_FIELD = "email";
	private static final String MSG_FIELD = "mensagem";
	private static final String ASSUNTO_FIELD = "assunto";
	
	
	FuncionarioController() {
	}

	@SuppressWarnings("unused")
	@OpenApi(
            summary = "Realiza um pedido de conserto de bicicleta",
            operationId = "consertaBicicleta",
            path = "/consertaBicicleta",
            method = HttpMethod.POST,
            queryParams = {
            		@OpenApiParam(name = "idFuncionario", type = String.class, description = "ID do Funcionario"),
            		@OpenApiParam(name = "idBicicleta", type = String.class, description = "ID da Bicicleta"),
            		@OpenApiParam(name = "idTranca", type = String.class, description = "ID da Tranca"),
            },
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "409", content = {@OpenApiContent(from = ErrorResponse.class)}),
            }
    )
	public static void consertaBicicleta(Context ctx) {
    	int idFuncionario = getIdFuncionarioIntByCtx(ctx);
    	int idBicicleta = getIdBicicletaIntByCtx(ctx);
    	int idTranca = getIdTrancaIntByCtx(ctx);

    	FuncionarioDto funcionario = FuncionarioService.findById(idFuncionario);
    	
    	if(funcionario == null) {
            throw new NotFoundResponse(USER_NOT_FOUND);
    	}
    	
    	HttpResponse<String> responseLiberacao= Unirest.patch("https://javalin-heroku-equipamento.herokuapp.com/liberaTranca")
    	        .queryString("trancaId", idTranca)
    	        .asString();

    	HttpResponse<Bicicleta> responseStatus= Unirest.post("https://bicicletario.herokuapp.com/statusBicicleta")
    	        .queryString("id", idBicicleta)
    	        .queryString("status", "Em_reparo")
    	        .asObject(Bicicleta.class);

    	HttpResponse<Bicicleta> response= Unirest.get("https://bicicletario.herokuapp.com/bicicleta")
    	        .queryString("id", idBicicleta)
    	        .asObject(Bicicleta.class);
    	Bicicleta bicicleta = response.getBody();

    	LogRetiradaDto logRetirada = OperacoesService.saveRetirada(idFuncionario, bicicleta.code);
    	
        JSONObject json = new JSONObject();
        json.put(EMAIL_FIELD, EMAIL_TO_SEND)
            .put(MSG_FIELD, logRetirada.toString())
            .put(ASSUNTO_FIELD, "Bicicleta " + EM_REPARO);
    	
    	HttpResponse<String> responseEmail = Unirest.post(URL_HEROKU_EMAIL).body(json).asString();

    	ctx.status(200);
    	ctx.json(bicicleta);
    }

	@SuppressWarnings("unused")
	@OpenApi(
            summary = "Realiza um pedido de conserto de totem",	
            operationId = "consertaTotem",
            path = "/consertaTotem",
            method = HttpMethod.POST,
            queryParams = {
            		@OpenApiParam(name = "idFuncionario", type = String.class, description = "ID do Funcionario"),
            		@OpenApiParam(name = "idTotem", type = String.class, description = "ID do totem"),
            },
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "409", content = {@OpenApiContent(from = ErrorResponse.class)}),
            }
    )
    public static void consertaTotem(Context ctx) {
		int idFuncionario = getIdFuncionarioIntByCtx(ctx);
    	int idTotem = getIdTotemIntByCtx(ctx);
    	
    	FuncionarioDto funcionario = FuncionarioService.findById(idFuncionario);
    	
    	if(funcionario == null) {
            throw new NotFoundResponse(USER_NOT_FOUND);
    	}
    	
    	Totem.updateStatus(idTotem, EM_REPARO);
    	
    	HttpResponse<Totem> response= Unirest.get("https://javalin-heroku-equipamento.herokuapp.com/totem/buscaPorId")
    	        .queryString("totemId", idTotem)
    	        .asObject(Totem.class);
    	Totem totem = response.getBody();

    	LogRetiradaDto logRetirada = OperacoesService.saveRetirada(idFuncionario, totem.numero);

    	JSONObject json = new JSONObject();
        json.put(EMAIL_FIELD, EMAIL_TO_SEND)
            .put(MSG_FIELD, logRetirada.toString())
            .put(ASSUNTO_FIELD, "Totem " + EM_REPARO);
        
    	HttpResponse<String> responseEmail = Unirest.post(URL_HEROKU_EMAIL).body(json).asString();

    	ctx.status(200);
    	ctx.json(totem);
    }

	@SuppressWarnings("unused")
	@OpenApi(
            summary = "Realiza um pedido de conserto de tranca",
            operationId = "consertaTranca",
            path = "/consertaTranca",
            method = HttpMethod.POST,
            queryParams = {
            		@OpenApiParam(name = "idFuncionario", type = String.class, description = "ID do Funcionario"),
            		@OpenApiParam(name = "idTranca", type = String.class, description = "ID da Tranca"),
            },
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "409", content = {@OpenApiContent(from = ErrorResponse.class)}),
            }
    )
    public static void consertaTranca(Context ctx) {
		int idFuncionario = getIdFuncionarioIntByCtx(ctx);
    	int idTranca = getIdTrancaIntByCtx(ctx);
    	
    	FuncionarioDto funcionario = FuncionarioService.findById(idFuncionario);
    	
    	if(funcionario == null) {
            throw new NotFoundResponse(USER_NOT_FOUND);
    	}
    	
    	HttpResponse<String> responseStatus = Unirest.patch("https://javalin-heroku-equipamento.herokuapp.com/statusTranca")
        .queryString("trancaId", idTranca)
        .queryString("statusTranca", EM_REPARO)
        .asString();
    	
    	HttpResponse<Tranca> response= Unirest.get("https://javalin-heroku-equipamento.herokuapp.com/tranca/buscaPorId")
    	        .queryString("trancaId", idTranca)
    	        .asObject(Tranca.class);
    	Tranca tranca = response.getBody();

    	LogRetiradaDto logRetirada = OperacoesService.saveRetirada(idFuncionario, tranca.numero);
    	
        JSONObject json = new JSONObject();
        json.put(EMAIL_FIELD, EMAIL_TO_SEND)
            .put(MSG_FIELD, logRetirada.toString())
            .put(ASSUNTO_FIELD, "Tranca " + EM_REPARO);
    	
    	HttpResponse<String> responseEmail = Unirest.post(URL_HEROKU_EMAIL).body(json).asString();
    	ctx.status(200);
    	ctx.json(tranca);
    }

	@SuppressWarnings("unused")
	@OpenApi(
            summary = "Realiza um pedido de aposentar bicicleta",
            operationId = "aposentaBicicleta",
            path = "/aposentaBicicleta",
            method = HttpMethod.POST,
            queryParams = {
            		@OpenApiParam(name = "idFuncionario", type = String.class, description = "ID do Funcionario"),
            		@OpenApiParam(name = "idBicicleta", type = String.class, description = "ID da Bicicleta"),
            		@OpenApiParam(name = "idTranca", type = String.class, description = "ID da Tranca"),
            },
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "409", content = {@OpenApiContent(from = ErrorResponse.class)}),
            }
    )
    public static void aposentaBicicleta(Context ctx) {
		int idFuncionario = getIdFuncionarioIntByCtx(ctx);
    	int idBicicleta = getIdBicicletaIntByCtx(ctx);
    	int idTranca = getIdTrancaIntByCtx(ctx);
    	
    	FuncionarioDto funcionario = FuncionarioService.findById(idFuncionario);
    	
    	if(funcionario == null) {
            throw new NotFoundResponse(USER_NOT_FOUND);
    	}
    	
    	HttpResponse<String> responseLiberacao= Unirest.patch("https://javalin-heroku-equipamento.herokuapp.com/liberaTranca")
    	        .queryString("trancaId", idTranca)
    	        .asString();

       	HttpResponse<Bicicleta> responseStatus= Unirest.post("https://bicicletario.herokuapp.com/statusBicicleta")
    	        .queryString("id", idBicicleta)
    	        .queryString("status", APOSENTADA)
    	        .asObject(Bicicleta.class);
       	    	
    	HttpResponse<Bicicleta> response= Unirest.get("https://bicicletario.herokuapp.com/bicicleta")
    	        .queryString("id", idBicicleta)
    	        .asObject(Bicicleta.class);
    	Bicicleta bicicleta = response.getBody();

    	LogRetiradaDto logRetirada = OperacoesService.saveRetirada(idFuncionario, bicicleta.code);
    	
        JSONObject json = new JSONObject();
        json.put(EMAIL_FIELD, EMAIL_TO_SEND)
            .put(MSG_FIELD, logRetirada.toString())
            .put(ASSUNTO_FIELD, "Bicicleta " + APOSENTADA);
        
    	HttpResponse<String> responseEmail = Unirest.post(URL_HEROKU_EMAIL).body(json).asString();

    	ctx.status(200);
    	ctx.json(bicicleta);
    }

	@SuppressWarnings("unused")
	@OpenApi(
            summary = "Realiza um pedido de aposentar totem",
            operationId = "aposentaTotem",
            path = "/aposentaTotem",
            method = HttpMethod.POST,
            queryParams = {
            		@OpenApiParam(name = "idFuncionario", type = String.class, description = "ID do Funcionario"),
            		@OpenApiParam(name = "idTotem", type = String.class, description = "ID do Totem"),
            },
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "409", content = {@OpenApiContent(from = ErrorResponse.class)}),
            }
    )
    public static void aposentaTotem(Context ctx) {
		int idFuncionario = getIdFuncionarioIntByCtx(ctx);
    	int idTotem = getIdTotemIntByCtx(ctx);
    	
    	FuncionarioDto funcionario = FuncionarioService.findById(idFuncionario);
    	
    	if(funcionario == null) {
            throw new NotFoundResponse(USER_NOT_FOUND);
    	}
    	    	
    	Totem.updateStatus(idTotem, APOSENTADA);
    	
    	HttpResponse<Totem> response= Unirest.get("https://javalin-heroku-equipamento.herokuapp.com/totem/buscaPorId")
    	        .queryString("totemId", idTotem)
    	        .asObject(Totem.class);
    	Totem totem = response.getBody();

    	LogRetiradaDto logRetirada = OperacoesService.saveRetirada(idFuncionario, totem.numero);
    	
        JSONObject json = new JSONObject();
        json.put(EMAIL_FIELD, EMAIL_TO_SEND)
            .put(MSG_FIELD, logRetirada.toString())
            .put(ASSUNTO_FIELD, "Totem " + APOSENTADA);

    	HttpResponse<String> responseEmail = Unirest.post(URL_HEROKU_EMAIL).body(json).asString();
    	ctx.status(200);
    	ctx.json(totem);

    }
    
	@SuppressWarnings("unused")
	@OpenApi(
            summary = "Realiza um pedido de aposentar tranca",
            operationId = "aposentaTranca",
            path = "/aposentaTranca",
            method = HttpMethod.POST,
            queryParams = {
            		@OpenApiParam(name = "idFuncionario", type = String.class, description = "ID do Funcionario"),
            		@OpenApiParam(name = "idTranca", type = String.class, description = "ID da Tranca"),
            },
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "409", content = {@OpenApiContent(from = ErrorResponse.class)}),
            }
    )
    public static void aposentaTranca(Context ctx) {
		int idFuncionario = getIdFuncionarioIntByCtx(ctx);
    	int idTranca = getIdTrancaIntByCtx(ctx);
    	
    	FuncionarioDto funcionario = FuncionarioService.findById(idFuncionario);
    	
    	if(funcionario == null) {
            throw new NotFoundResponse(USER_NOT_FOUND);
    	}
    	     	
     	HttpResponse<String> responseStatus = Unirest.patch("https://javalin-heroku-equipamento.herokuapp.com/statusTranca")
        .queryString("trancaId", idTranca)
        .queryString("statusTranca", APOSENTADA)
        .asString();
    	
       	HttpResponse<Tranca> response= Unirest.get("https://javalin-heroku-equipamento.herokuapp.com/tranca/buscaPorId")
    	        .queryString("trancaId", idTranca)
    	        .asObject(Tranca.class);

    	Tranca tranca = response.getBody();

    	LogRetiradaDto logRetirada = OperacoesService.saveRetirada(idFuncionario, tranca.numero);
    	
        JSONObject json = new JSONObject();
        json.put(EMAIL_FIELD, EMAIL_TO_SEND)
            .put(MSG_FIELD, logRetirada.toString())
            .put(ASSUNTO_FIELD, "Tranca " + APOSENTADA);

    	HttpResponse<String> responseEmail = Unirest.post(URL_HEROKU_EMAIL).body(json).asString();

    	ctx.status(200);
    	ctx.json(tranca);

    }
    
    @OpenApi(
            summary = "retorna funcionarios cadastrados",
            operationId = "getFuncionario",
            path = "/funcionario",
            method = HttpMethod.GET,
            queryParams = {
            		@OpenApiParam(name = "idFuncionario", type = String.class, description = "Busca por id"),
            		@OpenApiParam(name = "nome", type = String.class, description = "Busca por nome"),
            		@OpenApiParam(name = "matricula", type = Integer.class, description = "Busca por matricula")
			},
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = FuncionarioDto.class)})
            }
    )
    public static void getFuncionario(Context ctx) {
    	int id = getIdFuncionarioIntByCtx(ctx);
    	String nome = getNomeByCtx(ctx);
    	long matricula = getMatriculaByCtx(ctx);
    	Collection<FuncionarioDto> funcionarios = new ArrayList<>();
    	
    	if(id != 0)
    	{
    		FuncionarioDto funcionario = FuncionarioService.findById(id);
    		if(funcionario != null) {
            	funcionarios.add(funcionario);
    		}
    	}
    	else if(matricula != 0)
    	{
    		FuncionarioDto funcionario = FuncionarioService.findByMatricula(matricula);
    		if(funcionario != null) {
            	funcionarios.add(funcionario);
    		}
    	}
    	else if (nome != null)
    	{
    		FuncionarioDto funcionario = FuncionarioService.findByNome(nome);
    		if(funcionario != null) {
            	funcionarios.add(funcionario);	
    		}
    	}
    	else
    	{
    		funcionarios = FuncionarioService.getAllFuncionarios();
    	}
         if (funcionarios.isEmpty()) {
             throw new NotFoundResponse(USER_NOT_FOUND);
         } else {
             ctx.status(200);
         }
         
         ctx.json(funcionarios);
    }
    
    @OpenApi(
            summary = "Deleta um funcionario cadastrado pelo ID",
            operationId = "deleteFuncionario",
            path = "/funcionario",
            method = HttpMethod.DELETE,
            queryParams = {
            		@OpenApiParam(name = "idFuncionario", type = String.class, description = "id"),
			},
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = FuncionarioDto.class)})
            }
    )
    public static void deleteFuncionario(Context ctx) {
    	int id = getIdFuncionarioIntByCtx(ctx);
    	FuncionarioDto funcionario = FuncionarioService.findById(id);
         if (funcionario == null) {
             throw new NotFoundResponse(USER_NOT_FOUND);
         } else {
        	 FuncionarioService.deleteFuncionario(id);
             ctx.status(200);
         	ctx.json(funcionario);
         }
     }
 
    @OpenApi(
            summary = "Adiciona um novo cadastro de funcionario",
            operationId = "createFuncionario",
            path = "/funcionario",
            method = HttpMethod.POST,
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewFuncionarioRequest.class)}),
            tags = {"Funcionario"},
            responses = {
                    @OpenApiResponse(status = "201", content = {@OpenApiContent(from = FuncionarioDto.class)}),
                    @OpenApiResponse(status = "409", content = {@OpenApiContent(from = ErrorResponse.class)}),
            }
    )
    public static void createFuncionario(Context ctx) {
    	NewFuncionarioRequest funcionario = ctx.bodyAsClass(NewFuncionarioRequest.class);
    	
    	FuncionarioService.saveFuncionario(funcionario);
    	ctx.status(200);
    	ctx.json(funcionario);
    }
    
    private static int getIdFuncionarioIntByCtx(Context ctx)
    {
    	int id;
        String idString = ctx.queryParam("idFuncionario");
        if(idString != null)
        {
        	id = Integer.parseInt(idString);
        }
        else
        {
        	id = 0;
        }
        return id;
    }
    
    private static String getNomeByCtx(Context ctx)
    {
        return ctx.queryParam("nome");
    }
    
    private static long getMatriculaByCtx(Context ctx)
    {    	
    	long matricula;
        String matriculaString =  ctx.queryParam("matricula");
        if(matriculaString != null)
        {
        	matricula = Integer.parseInt(matriculaString);
        }
        else
        {
        	matricula = 0;
        }
        return matricula;
    }
    
    private static int getIdBicicletaIntByCtx(Context ctx)
    {
        String idString = ctx.queryParam("idBicicleta");
        return Integer.parseInt(idString);
    }
    
    private static int getIdTotemIntByCtx(Context ctx)
    {
        String idString = ctx.queryParam("idTotem");
        return Integer.parseInt(idString);
    }
    
    private static int getIdTrancaIntByCtx(Context ctx)
    {
        String idString = ctx.queryParam("idTranca");
        return Integer.parseInt(idString);
    }


 }
