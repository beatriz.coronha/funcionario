package funcionario;

import java.util.logging.Level; 
import java.util.logging.Logger;


public class Main {

    public static void main(String[] args) {
    	
   	 JavalinApp app = new JavalinApp();    	    
   	 int port = getHerokuAssignedPort();
   	 
   	 app.start(port);

        Logger logger = Logger.getLogger(Main.class.getName());
        logger.log(Level.INFO, "Check out ReDoc docs at http://localhost:7000/redoc");
        logger.log(Level.INFO, "Check out Swagger UI docs at http://localhost:7000/swagger-ui");
    }
    
    private static int getHerokuAssignedPort() {
        String herokuPort = System.getenv("PORT");
        if (herokuPort != null) {
          return Integer.parseInt(herokuPort);
        }
        return 7000;
      }

}
