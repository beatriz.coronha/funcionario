package funcionario;

import funcionario.dtos.DocumentoDto;

public class NewFuncionarioRequest {
	public String nome;
	public long matricula;
	public boolean indicadorBr;
	public DocumentoDto documento;
	
	public NewFuncionarioRequest() {
		
	}
	
	public NewFuncionarioRequest(String nome, long matricula, boolean indicadorBr, DocumentoDto documento) {
		this.nome = nome;
		this.matricula = matricula;
		this.indicadorBr = indicadorBr;
		this.documento = documento;
	}


}
