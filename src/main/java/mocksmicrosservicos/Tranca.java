package mocksmicrosservicos;

import java.util.HashMap;
import java.util.Map;

public class Tranca {
	public String id;
	public String status;
	public long numero;
	
    private static Map<Integer, Tranca> trancas = new HashMap<>();

    static {
    	trancas.put(1, new Tranca("1", "Em uso", 123));
    }
	
    Tranca(String id, String status, long numero)
	{
		this.id = id;
		this.status = status;
		this.numero = numero;
	}
	
	
	public static Tranca findById(int id)
	{
        return trancas.get(id);	
	}
	
	public static void updateStatus(int id, String status)
	{
		Tranca tranca = trancas.get(id);	
		tranca.status = status;
	}

}
