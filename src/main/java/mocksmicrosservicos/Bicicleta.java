package mocksmicrosservicos;

import java.util.HashMap;
import java.util.Map;

public class Bicicleta {
	public String id;
	public String status;
	public long code;
	
    private static Map<Integer, Bicicleta> bicicletas = new HashMap<>();

    static {
    	bicicletas.put(1, new Bicicleta("1", "Em uso", 123));
    }
	
	Bicicleta(String id, String status, long numero)
	{
		this.id = id;
		this.status = status;
		this.code = numero;
	}
	
	
	public static Bicicleta findById(int id)
	{
        return bicicletas.get(id);	
	}
	
	public static void updateStatus(int id, String status)
	{
        Bicicleta bicicleta = bicicletas.get(id);	
        bicicleta.status = status;
	}
	

}
