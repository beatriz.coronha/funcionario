package mocksmicrosservicos;

import java.util.HashMap;
import java.util.Map;

public class Totem {
	public String id;
	public String status;
	public long numero;
	
    private static Map<Integer, Totem> totens = new HashMap<>();
 
    static {
    	totens.put(1, new Totem("1", "Em uso", 123));
    }
	
    Totem(String id, String status, long numero)
	{
		this.id = id;
		this.status = status;
		this.numero = numero;
	}
	
	
	public static Totem findById(int id)
	{
        return totens.get(id);	
	}
	
	public static void updateStatus(int id, String status)
	{
		Totem totem = totens.get(id);	
		totem.status = status;
	}

}
