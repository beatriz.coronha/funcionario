

import org.junit.jupiter.api.Test;

import funcionario.JavalinApp;
import kong.unirest.Unirest;
import static org.junit.jupiter.api.Assertions.*;



public class FunctionalTest {
	 
	 @Test
		public void testGetAllFuncionarios() {
		 JavalinApp app = new JavalinApp();
		 app.start(1111);
	        //String funcionariosJson = "[{\"id\":\"1\",\"nome\":\"Alice\",\"matricula\":0,\"indicadorBr\":true,\"documento\":null},{\"id\":\"2\",\"nome\":\"Bob\",\"matricula\":111,\"indicadorBr\":true,\"documento\":null},{\"id\":\"3\",\"nome\":\"Bia\",\"matricula\":123,\"indicadorBr\":true,\"documento\":{\"tipo\":\"CPF\",\"numero\":2,\"foto\":\"foto\"}},{\"id\":\"5\",\"nome\":\"Beatriz\",\"matricula\":12345678,\"indicadorBr\":true,\"documento\":null},{\"id\":\"6\",\"nome\":\"Carlos\",\"matricula\":444,\"indicadorBr\":true,\"documento\":null}]>";
	        kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1111/funcionario").asString();
	      	assertEquals(200, response.getStatus());
	        //assertEquals(funcionariosJson, response.getBody());
		    app.stop();
		}
	 
	 @Test
	    public void testGetFuncionariosById() {
		 JavalinApp app = new JavalinApp();
	    	app.start(2222);
	        String funcionariosJson = "[{\"id\":\"1\",\"nome\":\"Alice\",\"matricula\":0,\"indicadorBr\":true,\"documento\":null}]"; 	
	        kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:2222/funcionario?idFuncionario=1").asString();
	      	assertEquals(200, response.getStatus());
	        assertEquals(funcionariosJson, response.getBody());
		    app.stop();
	    }
	    
	    @Test
	    public void testGetFuncionariosByNome() {
			 JavalinApp app = new JavalinApp();
	    	app.start(3333);
	        String funcionariosJson = "[{\"id\":\"1\",\"nome\":\"Alice\",\"matricula\":0,\"indicadorBr\":true,\"documento\":null}]";    	
	        kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:3333/funcionario?nome=Alice").asString();
	      	assertEquals(200, response.getStatus());
	        assertEquals(funcionariosJson, response.getBody());
		    app.stop();
	    }
	    
	    @Test
	    public void testGetFuncionariosByMatricula() {
			 JavalinApp app = new JavalinApp();
	    	app.start(4444);
	        String funcionariosJson = "[{\"id\":\"2\",\"nome\":\"Bob\",\"matricula\":111,\"indicadorBr\":true,\"documento\":null}]";    	
	        kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:4444/funcionario?matricula=111").asString();
	      	assertEquals(200, response.getStatus());
	        assertEquals(funcionariosJson, response.getBody());
		    app.stop();
	    }
	    
	    @Test
	    public void testGetFuncionariosError() {
			 JavalinApp app = new JavalinApp();
	    	app.start(5555);
	        kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:5555/funcionario?idFuncionario=15").asString();
	      	assertEquals(404, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testDELETEfuncionarios() {
			 JavalinApp app = new JavalinApp();
	    	app.start(6666);
	        kong.unirest.HttpResponse<String> response = Unirest.delete("http://localhost:6666/funcionario?idFuncionario=2").asString();
	      	assertEquals(200, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testDELETEfuncionariosError() {
			 JavalinApp app = new JavalinApp();
	    	app.start(7777);
	        kong.unirest.HttpResponse<String> response = Unirest.delete("http://localhost:7777/funcionario?idFuncionario=56").asString();
	      	assertEquals(404, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTFuncionarios() {
			 JavalinApp app = new JavalinApp();
	    	app.start(8888);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:8888/funcionario").body("{\"nome\":\"Bia\",\"matricula\":123,\"indicadorBr\":true,\"documento\":{\"tipo\":\"CPF\",\"numero\":2,\"foto\":\"foto\"}}").asString();
	      	assertEquals(200, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTAposentaBicicleta() {
			 JavalinApp app = new JavalinApp();
	    	app.start(9999);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:9999/aposentaBicicleta?idFuncionario=1&idBicicleta=1&idTranca=1").asString();
	      	assertEquals(200, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTAposentaBicicletaUserNotFound() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1010);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1010/aposentaBicicleta?idFuncionario=15&idBicicleta=1&idTranca=1").asString();
	      	assertEquals(404, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTAposentaTranca() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1212);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1212/aposentaTranca?idFuncionario=1&idTranca=1").asString();
	      	assertEquals(200, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTAposentaTrancaUserNotFound() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1313);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1313/aposentaTranca?idFuncionario=15&idTranca=1").asString();
	      	assertEquals(404, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTAposentaTotem() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1414);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1414/aposentaTotem?idFuncionario=1&idTotem=1").asString();
	      	assertEquals(200, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTAposentaTotemUserNotFound() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1515);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1515/aposentaTotem?idFuncionario=15&idTotem=1").asString();
	      	assertEquals(404, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTConsertaBicicleta() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1616);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1616/consertaBicicleta?idFuncionario=1&idBicicleta=1&idTranca=1").asString();
	      	assertEquals(200, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTConsertaBicicletaUserNotFound() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1717);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1717/consertaBicicleta?idFuncionario=15&idBicicleta=1&idTranca=1").asString();
	      	assertEquals(404, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTConsertaTranca() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1818);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1818/consertaTranca?idFuncionario=1&idTranca=1").asString();
	      	assertEquals(200, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTConsertaTrancaUserNotFound() {
			 JavalinApp app = new JavalinApp();
	    	app.start(1919);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1919/consertaTranca?idFuncionario=15&idTranca=1").asString();
	      	assertEquals(404, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTConsertaTotem() {
			 JavalinApp app = new JavalinApp();
	    	app.start(2020);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:2020/consertaTotem?idFuncionario=1&idTotem=1").asString();
	      	assertEquals(200, response.getStatus());
		    app.stop();
	    }
	    
	    @Test
	    public void testPOSTAConsertaTotemUserNotFound() {
			 JavalinApp app = new JavalinApp();
	    	app.start(2121);
	        kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:2121/consertaTotem?idFuncionario=15&idTotem=1").asString();
	      	assertEquals(404, response.getStatus());
		    app.stop();
	    }
	
}
