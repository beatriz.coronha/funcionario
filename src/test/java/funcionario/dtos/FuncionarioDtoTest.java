package funcionario.dtos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import funcionario.daos.LogRetiradaDao;

public class FuncionarioDtoTest {

	@Test
	public void testEmptyConstructor() {
		FuncionarioDto funcionarioDto = new FuncionarioDto();
		
		assertNotNull(funcionarioDto);
	}
	
	@Test
	public void testfilledConstructor() {
		FuncionarioDto funcionarioDto = new FuncionarioDto(
															"1",
															"Beatriz",
															123,
															true,
															null);
		
		assertEquals("1", funcionarioDto.id);
		assertEquals("Beatriz", funcionarioDto.nome);
		assertEquals(123, funcionarioDto.matricula);
		assertTrue(funcionarioDto.indicadorBr);
		assertNull(funcionarioDto.documento);
	}
}
