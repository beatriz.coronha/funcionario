package funcionario.dtos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class DocumentoDtoTest {

	@Test
	public void testEmptyConstructor() {
		DocumentoDto documentoDto = new DocumentoDto();
		
		assertNotNull(documentoDto);
	}
	
	@Test
	public void testfilledConstructor() {
		DocumentoDto documentoDto = new DocumentoDto(
															"CPF",
															123,
															"foto.jpeg"
													);
		
		assertEquals("CPF", documentoDto.tipo);
		assertEquals(123, documentoDto.numero);
		assertEquals("foto.jpeg", documentoDto.foto);
	}
}
