package funcionario;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NewFuncionarioRequestTest {

	@Test
	public void testEmptyConstructor() {
		NewFuncionarioRequest funcRequest = new NewFuncionarioRequest();
		
		assertNotNull(funcRequest);
	}
	
	@Test
	public void testfilledConstructor() {
		NewFuncionarioRequest funcRequest = new NewFuncionarioRequest(
															"Beatriz",
															123,
															true,
															null);
		
		assertEquals("Beatriz", funcRequest.nome);
		assertEquals(123, funcRequest.matricula);
		assertTrue(funcRequest.indicadorBr);
		assertNull(funcRequest.documento);
	}

}
