package funcionario.daos;
import org.junit.jupiter.api.Test;
import funcionario.dtos.FuncionarioDto;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class FuncionarioDaoTest {
    
    //@Test
    //public void testSave() {
    //	NewFuncionarioRequest newFuncionario = new NewFuncionarioRequest(
    //																	"Beatriz",
    //																	1234578,
    //																	true,
    //																	null);
  
    //	int sizeBefore = FuncionarioDao.getFuncionarios().size();
    	
    //	FuncionarioDao.save(newFuncionario);
    	
    //	int sizeAfter = FuncionarioDao.getFuncionarios().size();
    //	assertEquals(sizeAfter, sizeBefore + 1);
    //}
    
	@Test
	public void testGetById() {
	        int expectedId = 2;
			FuncionarioDto funcionarioDto = FuncionarioDao.getById(expectedId);
			int resultId = Integer.valueOf(funcionarioDto.id);
	        assertEquals(expectedId, resultId);
	    }
    
	@Test
    public void testDelete() {
    	int sizeBefore = FuncionarioDao.getFuncionarios().size();
    	
    	FuncionarioDao.delete(4);
    	int sizeAfter = FuncionarioDao.getFuncionarios().size();
    	assertEquals(sizeAfter, sizeBefore - 1);
    	}
	
	@Test
	public void testGetAll() {
		Collection<FuncionarioDto> expected = FuncionarioDao.getFuncionarios().values();
		Collection<FuncionarioDto> result = FuncionarioDao.getAll();
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testSetFuncionarios() {
	    Map<Integer, FuncionarioDto> funcionariosExpected = new HashMap<>();
	    funcionariosExpected.put(1, new FuncionarioDto("1", "Alice", 000, true, null));
	    funcionariosExpected.put(2, new FuncionarioDto("2", "Bob", 111, true, null));
	    funcionariosExpected.put(3, new FuncionarioDto("3", "Carol", 222, true, null));
	    funcionariosExpected.put(4, new FuncionarioDto("4", "Dave", 333, true, null));
	    funcionariosExpected.put(5, new FuncionarioDto("5", "Beatriz", 12345678, true, null));
	    funcionariosExpected.put(6, new FuncionarioDto("6", "Carlos", 444, true, null));
	    FuncionarioDao.setFuncionarios(funcionariosExpected);
	    
	    Map<Integer, FuncionarioDto> funcionariosResult = FuncionarioDao.getFuncionarios();
	    
	    assertEquals(funcionariosExpected, funcionariosResult);
	}
	
	
	@Test
	public void testSetLastId() {	
		AtomicInteger expected = new AtomicInteger(2);
		
		FuncionarioDao.setLastId(expected);
		
		AtomicInteger result = FuncionarioDao.getLastId();
		
		assertEquals(expected, result);	
	}
	
	@Test
	public void testConstructor() {
		FuncionarioDao funcionarioDao = new FuncionarioDao();
		
		assertNotNull(funcionarioDao);
	}
	
}