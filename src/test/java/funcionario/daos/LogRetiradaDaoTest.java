package funcionario.daos;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import funcionario.dtos.LogRetiradaDto;

public class LogRetiradaDaoTest {

	@Test
	public void testEmptyConstructor() {
		LogRetiradaDao logDao = new LogRetiradaDao();
		
		assertNotNull(logDao);
	}
	
	@Test
	public void testSave() {
		int sizeBefore = LogRetiradaDao.getLogsRetirada().size();
		
		LogRetiradaDao.save(123, 123);
		
		int sizeAfter = LogRetiradaDao.getLogsRetirada().size();
		assertEquals(sizeAfter, sizeBefore + 1);

	}

}
