package funcionario.controllers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;

import funcionario.NewFuncionarioRequest;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;

public class FuncionarioControllerTest {
    
	@Test
	public void testConsertaBicicleta() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("idFuncionario")).thenReturn("1");
        when(ctx.queryParam("idBicicleta")).thenReturn("1");
		when(ctx.queryParam("idTranca")).thenReturn("1");
        
        FuncionarioController.consertaBicicleta(ctx);

        verify(ctx).status(200);
	}
	
	@Test
	public void testConsertaBicicletaUserNotFound() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("idFuncionario")).thenReturn("12");
        when(ctx.queryParam("idBicicleta")).thenReturn("1");
		when(ctx.queryParam("idTranca")).thenReturn("1");
    	
    	NotFoundResponse thrown = assertThrows(
    			NotFoundResponse.class,
                () -> FuncionarioController.consertaBicicleta(ctx)
         );

         assertTrue(thrown.getMessage().contains("User not found"));
        }

		
	@Test
	public void testConsertaTotem() {
		Context ctx = mock(Context.class);
		
		when(ctx.queryParam("idFuncionario")).thenReturn("2");
		when(ctx.queryParam("idTotem")).thenReturn("1");
		  
		FuncionarioController.consertaTotem(ctx);
		  
		verify(ctx).status(200);
	}
	
	@Test
	public void testConsertaTotemaUserNotFound() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("idFuncionario")).thenReturn("12");
        when(ctx.queryParam("idTotem")).thenReturn("1");
    	
    	NotFoundResponse thrown = assertThrows(
    			NotFoundResponse.class,
                () -> FuncionarioController.consertaTotem(ctx)
         );

         assertTrue(thrown.getMessage().contains("User not found"));
        }
		 

	@Test
	public void testConsertaTranca() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("idFuncionario")).thenReturn("1");
        when(ctx.queryParam("idTranca")).thenReturn("1");

        FuncionarioController.consertaTranca(ctx);

        verify(ctx).status(200);
	}
	
	@Test
	public void testConsertaTrancaaUserNotFound() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("idFuncionario")).thenReturn("12");
        when(ctx.queryParam("idTranca")).thenReturn("1");
    	
    	NotFoundResponse thrown = assertThrows(
    			NotFoundResponse.class,
                () -> FuncionarioController.consertaTranca(ctx)
         );

         assertTrue(thrown.getMessage().contains("User not found"));
        }

	
	  @Test
	  public void testAposentaBicicleta() {
		Context ctx = mock(Context.class);
		when(ctx.queryParam("idFuncionario")).thenReturn("1");
		when(ctx.queryParam("idBicicleta")).thenReturn("1");
		when(ctx.queryParam("idTranca")).thenReturn("1");
		  
		FuncionarioController.aposentaBicicleta(ctx);
		  
		verify(ctx).status(200);
	  }
	  
		@Test
		public void testAposentaBicicletaUserNotFound() {
			Context ctx = mock(Context.class);

	        when(ctx.queryParam("idFuncionario")).thenReturn("12");
	        when(ctx.queryParam("idBicicleta")).thenReturn("1");
			when(ctx.queryParam("idTranca")).thenReturn("1");
	    	
	    	NotFoundResponse thrown = assertThrows(
	    			NotFoundResponse.class,
	                () -> FuncionarioController.aposentaBicicleta(ctx)
	         );

	         assertTrue(thrown.getMessage().contains("User not found"));
	        }

	  
	  @Test
	  public void testAposentaTotem() {
		Context ctx = mock(Context.class);
		when(ctx.queryParam("idFuncionario")).thenReturn("1");
		when(ctx.queryParam("idTotem")).thenReturn("1");
		  
		FuncionarioController.aposentaTotem(ctx);
		  
		verify(ctx).status(200);
	}
	  
		@Test
		public void testAposentaTotemUserNotFound() {
			Context ctx = mock(Context.class);

	        when(ctx.queryParam("idFuncionario")).thenReturn("12");
	        when(ctx.queryParam("idTotem")).thenReturn("1");
	    	
	    	NotFoundResponse thrown = assertThrows(
	    			NotFoundResponse.class,
	                () -> FuncionarioController.aposentaTotem(ctx)
	         );

	         assertTrue(thrown.getMessage().contains("User not found"));
	        }
		  
	@Test
	public void testAposentaTranca() {
		Context ctx = mock(Context.class);
		when(ctx.queryParam("idFuncionario")).thenReturn("1");
		when(ctx.queryParam("idTranca")).thenReturn("1");
		  
		FuncionarioController.aposentaTranca(ctx);
		  
		verify(ctx).status(200);
	  }
	
	@Test
	public void testAposentaTrancaUserNotFound() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("idFuncionario")).thenReturn("12");
        when(ctx.queryParam("idTranca")).thenReturn("1");
    	
    	NotFoundResponse thrown = assertThrows(
    			NotFoundResponse.class,
                () -> FuncionarioController.aposentaTranca(ctx)
         );

         assertTrue(thrown.getMessage().contains("User not found"));
        }
	 

	@Test
	public void testGetFuncionarioById() {
		Context ctx = mock(Context.class);
        when(ctx.queryParam("idFuncionario")).thenReturn("1");
        
        FuncionarioController.getFuncionario(ctx);
        
		verify(ctx).status(200);
	}
	
	@Test
	public void testGetFuncionarioByMatricula() {
		Context ctx = mock(Context.class);
        when(ctx.queryParam("matricula")).thenReturn("111");
        
        FuncionarioController.getFuncionario(ctx);
        
		verify(ctx).status(200);
		}
	
	@Test
	public void testGetFuncionarioByNome() {
		Context ctx = mock(Context.class);
        when(ctx.queryParam("nome")).thenReturn("Bob");
        
        FuncionarioController.getFuncionario(ctx);
        
		verify(ctx).status(200);
		}
	
	@Test
	public void testGetFuncionarioByIdNotExist() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("idFuncionario")).thenReturn("12");
    	
    	NotFoundResponse thrown = assertThrows(
    			NotFoundResponse.class,
                () -> FuncionarioController.getFuncionario(ctx)
         );

         assertTrue(thrown.getMessage().contains("User not found"));
	}
	
	@Test
	public void testGetFuncionarioByMatriculaNotExist() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("matricula")).thenReturn("69515");
    	
    	NotFoundResponse thrown = assertThrows(
    			NotFoundResponse.class,
                () -> FuncionarioController.getFuncionario(ctx)
         );

         assertTrue(thrown.getMessage().contains("User not found"));
		}
	
	@Test
	public void testGetFuncionarioByNomeNotExist() {
		Context ctx = mock(Context.class);

        when(ctx.queryParam("nome")).thenReturn("teste");
    	
    	NotFoundResponse thrown = assertThrows(
    			NotFoundResponse.class,
                () -> FuncionarioController.getFuncionario(ctx)
         );

         assertTrue(thrown.getMessage().contains("User not found"));
		}
	
	@Test
	public void testGetAll() {
		Context ctx = mock(Context.class);
        
        FuncionarioController.getFuncionario(ctx);
        
		verify(ctx).status(200);
		}
	
	  @Test
	  public void testDeleteFuncionario() {
		Context ctx = mock(Context.class);
		
		when(ctx.queryParam("idFuncionario")).thenReturn("3");
		FuncionarioController.deleteFuncionario(ctx);
		  
		verify(ctx).status(200);
	  }
	  
		@Test
		public void testDeleteFuncionarioUserNotFound() {
			Context ctx = mock(Context.class);

	        when(ctx.queryParam("idFuncionario")).thenReturn("12");
	    	
	    	NotFoundResponse thrown = assertThrows(
	    			NotFoundResponse.class,
	                () -> FuncionarioController.deleteFuncionario(ctx)
	         );

	         assertTrue(thrown.getMessage().contains("User not found"));
	        }

	  
	 @Test
	 public void testCreateFuncionario() {
		Context ctx = mock(Context.class);
		NewFuncionarioRequest newFuncionario = new NewFuncionarioRequest( "Beatriz", 123, true, null);
		 
		when(ctx.bodyAsClass(NewFuncionarioRequest.class)).thenReturn(newFuncionario);
	  
		FuncionarioController.createFuncionario(ctx);
		verify(ctx).status(200);
		}
	 
		@Test
		public void testEmptyConstructor() {
			FuncionarioController funcionarioController = new FuncionarioController();
			
			assertNotNull(funcionarioController);
		}

}
